# sparse-benchmarks



## Getting started

Setup the python virtual environement with the correct dependencies

```bash
# put the venv in this directory is optionnal
# only needed if you want linting from pyright
mkdir venvs
cd venvs
python3 -m venv sparse-benchmarks-venv
source sparse-benchmarks-venv/bin/activate
python -m pip install -r requirements.txt

# run the unit tests if you want
pytest benchmark/tests

# now bench can start
python benchmark
```
