from dataclasses import dataclass
from typing import Any
from statistics import median, stdev
from dataclasses import dataclass
import numpy as np


@dataclass
class BenchData:
    """Class for result of bench than can be written as a line in a csv."""

    framework: str
    network: str
    exec_times: list[int]
    outputs: list[Any]


def print_bench_summary(results: list[BenchData]) -> None:
    """
    Print content of dict results on a clean table way.
    Time are integer in nano seconds.
    """
    # 5 + (ncols - 1)*5 + ncols*15 + 5
    ncols = 4
    title = "Benchmark Summary"
    len_str = 5 * (ncols + 1) + 25 * ncols
    print(" " * ((len_str - len(title)) // 2) + title)
    print("-" * len_str)

    def print_line(*columns):
        assert len(columns) == ncols
        meta = f'{"|":^5s}'.join(map(lambda c: f"{c:^25s}", columns))
        fstr = f'{"|":<5s}' + meta + f'{"|":>5s}'
        print(fstr)

    print_line("Framework", "Network", "Median", "StDev")
    print("-" * len_str)
    for benchdata in results:
        ns = median(benchdata.exec_times)
        sec = f"{(ns / 10**6):.3f} ms"
        ns = stdev(benchdata.exec_times)
        sec2 = f"{(ns / 10**6):.3f} ms"
        net = benchdata.network.split("/")[-1].replace(".onnx", "")
        fra = benchdata.framework
        print_line(fra, net, sec, sec2)
    print("-" * len_str)


def validity_check(
    results: list[BenchData], epsilon: float = 0.001, n_max_tests: int = 200
) -> bool:
    refs = results[0].outputs
    frameworkr = results[0].framework
    for benchdata in results[1:]:
        # check that all evaluators have the same outputs
        o = benchdata.outputs
        frameworki = benchdata.framework
        # 10 repetitions
        for refi, oi in zip(refs, o):
            # 4 outputs
            for oref, oj in zip(refi, oi):
                # compare np.array
                if oref.shape != oj.shape:
                    print(
                        f"Validity Error: shapes not equal {oref.shape} in {frameworkr}, {oj.shape}, in {frameworki}"
                    )
                    return False
                size = oref.size
                if size == 1:
                    tocmp = [(oref.item(), oj.item())]
                elif size <= n_max_tests:
                    tocmp = zip(oref.flatten(), oj.flatten())
                else:
                    rdints = np.random.randint(0, size, n_max_tests)
                    tocmp = zip(oref.flatten()[rdints], oj.flatten()[rdints])
                for x, y in tocmp:
                    if abs(x - y) > epsilon:
                        print(
                            f"Validity error: {x=} in {frameworkr}!= {y=} in {frameworki}"
                        )
                        return False
    return True
