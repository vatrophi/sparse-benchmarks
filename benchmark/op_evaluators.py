import time
from typing import Any
from numpy._typing import NDArray
import torch
import numpy as np

from benchdata import BenchData


class OpEvaluator:
    @property
    def name(self) -> str:
        return "GenericOpEvaluator"

    def convert_input(self, inp: NDArray) -> Any:
        """Convert numpy array as input to the expected format by operator input"""
        raise NotImplemented

    def convert_to_sparse(self, dense_mat: NDArray) -> Any:
        """Convert numpy array to sparse format used by the operator."""
        raise NotImplemented

    def execute_op(self, sp_m, inp):
        raise NotImplemented

    def evaluate(
        self, shape: tuple[int, int], sparsity: float, repeat: int
    ) -> BenchData:
        assert 0.0 <= sparsity <= 1.0
        np.random.seed(42)
        print(f"Preparing benchmark of {self.name}...")
        # generate sparse matrix in numpy dense array
        sp_m = np.zeros(shape, dtype=np.float32)
        nnz = int(sp_m.size * sparsity)
        nnz_inds = np.random.randint(0, high=shape, size=(nnz, 2))
        sp_m[nnz_inds[:, 0], nnz_inds[:, 1]] = np.random.rand(nnz).astype(np.float32)
        sp_m = self.convert_to_sparse(sp_m)
        times = []
        outputs = []
        for i in range(1, 1 + repeat):
            # prepare input values for op
            inp = np.random.rand(shape[0], shape[1]).astype(np.float32)
            inp = self.convert_input(inp)
            print(f"Executing op {self.name} {i} / {repeat}")
            start = time.thread_time_ns()
            out = self.execute_op(sp_m, inp)
            end = time.thread_time_ns()
            times.append(end - start)
            outputs.append(out)
        return BenchData(self.name, "Matmul", times, outputs)


class TorchMatmulSpDenseEval(OpEvaluator):
    @property
    def name(self):
        """The name property."""
        return "TorchMatmulSpDense"

    def convert_input(self, inp: NDArray) -> Any:
        return torch.from_numpy(inp)

    def convert_to_sparse(self, dense_mat: NDArray) -> Any:
        return torch.from_numpy(dense_mat).to_sparse()

    def execute_op(self, sp_m, inp):
        return torch.matmul(sp_m, inp)
