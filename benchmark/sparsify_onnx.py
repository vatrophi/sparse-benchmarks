import numpy
import onnx
from onnx.helper import make_tensor, make_sparse_tensor
from onnx import numpy_helper, TensorProto, SparseTensorProto
from typing import Any, NamedTuple


class SparseConvertor(NamedTuple):
    """
    Class to hold necessary info to convert some initializer to its equivalent sparse_initializer.
    The necessary info are :
    - to_delete:Any reference to initializer to delete in the initializer list after the sparse one is created.
    - name: str the name of the tensor.
    - dims: list[int] shape of the tensor in a list.
    - indices: list[int] positions in the tensor of the nonzero values (e.g matrix indices = [i0,j0,i1,j1,...]).
    - values: list[int|float] nonzeros values (e.g matrix sparse_tens[indices[i]][indices[i+1]] = values[i]).
    - values: int an integer like TensorProto.FLOAT32 defined in onnx lib to encode type of values.
    """

    to_delete: Any
    name: str
    dims: list[int]
    indices: list[int]
    values: list[int | float]
    values_type: int

    def to_sparse_tensorproto(self) -> SparseTensorProto:
        values = make_tensor(
            self.name, self.values_type, [len(self.values)], self.values
        )
        n = len(self.dims)
        assert len(self.values) == len(self.indices) // n
        indices = make_tensor(
            "indices" + self.name,
            TensorProto.INT64,
            [len(self.indices) // n, n],
            self.indices,
        )
        return make_sparse_tensor(values, indices, self.dims)


def sparsify_onnx(path_src: str, path_dst: str, sparsity: float) -> None:
    """
    Convert an onnx file to another onnx file with sparse tensor initializer
    instead of dense one when a sparsity threshold is exceded.

    Warning:

        This function worked as intended and produce a valid onnx file as output.
        HOWEVER all the parser of onnx files into framework (tf, tvm, deepsparse)
        will IGNORE thes parse_initializer resulting in parsing errors or unknown behiaviors.

    Arguments:

    :param path_src: str path to the onnx file as input (must exist else an error will be raised).
    :param path_dst: str path to the onnx file as output (will be created if not already exist).
    :param sparsity: float minimum threshold of zeros / size(tensor) to trigger the conversion.
    """
    assert 0 <= sparsity <= 1
    model = onnx.load(path_src)
    convertors = []
    for init in model.graph.initializer:
        # init is a TensorProto that might be converted to SparseProto
        values_type = init.data_type
        a = numpy_helper.to_array(init)
        nonzeros = numpy.nonzero(a)
        # https://www.tensorflow.org/api_docs/python/tf/sparse/sparse_dense_matmul
        # - the density of A is larger than approximately 15%
        # - A dense fits in memory
        # - a lot of columns involved during product
        # if 2 or 3 yes then go for dense
        # for the moment we simply check the density
        n_nonzeros = len(nonzeros[0])
        dims = init.dims
        if dims and n_nonzeros / a.size < 1 - sparsity:
            name = init.name
            print(name, dims)
            indices = [ind[j] for j in range(n_nonzeros) for ind in nonzeros]
            values = a[nonzeros]
            convertor = SparseConvertor(init, name, dims, indices, values, values_type)
            convertors.append(convertor)

    for c in convertors:
        model.graph.initializer.remove(c.to_delete)
        sparse_init = c.to_sparse_tensorproto()
        model.graph.sparse_initializer.append(sparse_init)
    onnx.checker.check_model(model)
    onnx.save_model(model, path_dst)


if __name__ == "__main__":
    file = "yolov5s.onnx"
    root = "sparsezoo/"
    src = root + file
    dst = root + "sparsified" + file
    sparsify_onnx(src, dst, 0.8)
