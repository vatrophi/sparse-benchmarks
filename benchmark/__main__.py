#! ./venvs/sparse-benchmark-venv/bin/python
import os
from benchdata import BenchData, validity_check, print_bench_summary
from evaluators import (
    TensorflowEvaluator,
    TvmEvaluator,
    TvmSparseEvaluator,
    DeepSparseEvaluator,
)

from op_evaluators import TorchMatmulSpDenseEval


def benchmark_onnx(
    path: str, nrepeat: int = 10, validity: bool = True
) -> list[BenchData]:
    evaluators = [
        DeepSparseEvaluator(),
        TensorflowEvaluator(),
        TvmEvaluator(),
        TvmSparseEvaluator(),
    ]
    results = [e.evaluate_model(path, nrepeat) for e in evaluators]
    if validity and len(evaluators) >= 2:
        print("Checking validity...")
        is_ok = validity_check(results)
        s = "PASSED" if is_ok else "FAILED"
        print(f"Validity check {s}")

    return results


def main_onnx():
    rootdir = "sparsezoo"
    bench_res = []
    for onnx_file in os.listdir(rootdir):
        assert onnx_file[-5:] == ".onnx"
        bench_res += benchmark_onnx(f"{rootdir}/{onnx_file}")
    print_bench_summary(bench_res)


def main_op():
    shape = (100, 100)
    sparsity = 0.8
    repeat = 10
    b = TorchMatmulSpDenseEval().evaluate(shape, sparsity, repeat)
    print_bench_summary([b])
    # todo: do other ones and check validity


if __name__ == "__main__":
    # main_onnx()
    main_op()
