from benchmark.benchdata import validity_check, BenchData
import numpy as np


def test_validity_check():
    n_repeat = 10
    n_out = 4
    # generate fake time in ns
    times0 = [(n_repeat - i) * 10**9 for i in range(n_repeat)]
    times1 = [i + (n_repeat - i) * 10**9 for i in range(n_repeat)]
    # generate differents outputs here they are shifted from 1
    outputs0 = [[np.arange(10) for _ in range(n_out)] for _ in range(n_repeat)]
    outputs1 = [[1 + np.arange(10) for _ in range(n_out)] for _ in range(n_repeat)]
    fake_data = [
        BenchData("Test0", "yolotest", times0, outputs0),
        BenchData("Test1", "yolotest", times1, outputs1),
    ]

    assert not validity_check(fake_data)
