import time
from typing import Any
from numpy._typing import NDArray
import onnx
from onnx_tf.backend import prepare
import tensorflow as tf
import tvm
from tvm import relay
import deepsparse
from benchdata import BenchData
import numpy as np


InputDict = dict[str, list[int]] | dict[str, NDArray]


def input_shapes_from_onnx(model) -> InputDict:
    # model is a protobuf object i.e serialized json
    # it should have a graph.input field containing a list of inputs
    # each input has a name and also info about its shape inside type.tensor_type.shape.dim
    # these info are key value pairs with "dim_value":10 or "dim_param":"batch"
    # in the second case it will try to find what number put inside at runtime
    # since we dont care about this when benchmarking we just replace it by 1
    return {
        i.name: [
            d.dim_value if d.HasField("dim_value") else 1
            for d in i.type.tensor_type.shape.dim
        ]
        for i in model.graph.input
    }


class OnnxEvaluator:
    @property
    def framework(self) -> str:
        raise NotImplemented

    def load(self, path) -> tuple[Any, InputDict]:
        """Load model using framework own frontend and return it with its input shapes."""
        raise NotImplemented

    def run(self, prepared, input_values: InputDict) -> Any:
        raise NotImplemented

    def prepare(self, model) -> Any:
        raise NotImplemented

    def evaluate_model(self, path: str, repeat: int) -> BenchData:
        """
        Default implementation of framework evaluation applied on one model.

        load onnx model
        compile it
        generate inputs filled with ones
        run it & time it

        :param path: str path to the onnx file containing the model.
        :param repeat: int the number of time
        :return res: BenchData with numbers of nano seconds needed by the model to infer.
        """
        assert repeat > 0
        print(f"{self.framework} is loading {path}...")
        model, input_shapes = self.load(path)
        print(f"{self.framework} is compiling {path}...")
        prepared = self.prepare(model)
        np.random.seed(42)
        times = list(range(repeat))
        print(f"{self.framework} is executing {path} {repeat} times ...")
        outputs = [None for _ in range(repeat)]
        for i in range(repeat):
            input_values = {
                x: np.random.rand(*s).astype(np.float32)
                for x, s in input_shapes.items()
            }
            print(f"executing {i+1} / {repeat}")
            start = time.thread_time_ns()
            output = self.run(prepared, input_values)
            end = time.thread_time_ns()
            times[i] = end - start
            outputs[i] = output
        return BenchData(self.framework, path, times, outputs)


class TensorflowEvaluator(OnnxEvaluator):
    @property
    def framework(self) -> str:
        return "Tensorflow"

    def load(self, path) -> tuple[Any, InputDict]:
        model = onnx.load(path)
        shapes = input_shapes_from_onnx(model)
        return model, shapes

    def run(self, prepared, input_values) -> Any:
        return prepared.run(input_values)

    def prepare(self, model) -> Any:
        return prepare(model)


class TvmEvaluator(OnnxEvaluator):
    @property
    def framework(self) -> str:
        return "TVM"

    def load(self, path) -> tuple[Any, InputDict]:
        # we are copying the logic from tvmc.load when using onnx
        # we are kind of forced to do it to get the shapes
        # if shapes are unknown before compile tvm crash
        model = onnx.load(path)
        shapes = input_shapes_from_onnx(model)
        model, params = relay.frontend.from_onnx(model, shape=shapes)
        return ((model, params), shapes)

    def run(self, prepared, input_values: InputDict) -> Any:
        for name, arr in input_values.items():
            prepared.set_input(name, arr)
        prepared.run()
        return [
            prepared.get_output(i).numpy() for i in range(prepared.get_num_outputs())
        ]

    def prepare(self, model) -> Any:
        model, params = model
        with tvm.transform.PassContext(opt_level=3):
            lib = relay.build(model, target="llvm", params=params)
        model = tvm.contrib.graph_executor.GraphModule(lib["default"](tvm.cpu()))
        return model


class TvmSparseEvaluator(TvmEvaluator):
    @property
    def framework(self):
        """The framework property."""
        return "TVMsparse"

    def prepare(self, model) -> Any:
        m, p = model
        m, p = relay.data_dep_optimization.simplify_fc_transpose.convert(m["main"], p)
        m, p = relay.data_dep_optimization.bsr_dense.convert(
            m, p, (1, 1), sparsity_threshold=0.8
        )
        return super().prepare((m, p))


class DeepSparseEvaluator(OnnxEvaluator):
    @property
    def framework(self) -> str:
        return "DeepSparse"

    def load(self, path) -> tuple[Any, InputDict]:
        model = onnx.load(path)
        shapes = input_shapes_from_onnx(model)
        model = deepsparse.compile_model(path)
        return model, shapes

    def prepare(self, model) -> Any:
        return model

    def run(self, prepared, input_values: InputDict) -> Any:
        # might get issue with network using severals inputs
        # a note in the doc of the run function said that arrays should be contiguous in memory
        # np.ascontiguiousarray could be used to solve the issue
        return prepared.run(list(input_values.values()))
